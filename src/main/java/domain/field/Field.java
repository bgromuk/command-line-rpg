package domain.field;

import config.EnemyConfiguration;
import domain.characters.Character;
import domain.characters.Coordinates;
import domain.characters.Enemy;
import domain.characters.NPC;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static util.IntUtils.*;

public class Field implements Serializable {
    public static final int DEFAULT_FLD_LENGTH = 8;
    public static final int DEFAULT_FLD_WIDTH = 8;
    private final String name;
    private final Map<Coordinates, Enemy> enemies;

    private final Character[][] map;

    public Field(List<EnemyConfiguration> enemyConfiguration) {
        this.name = "Stinky town";
        this.enemies = new HashMap<>();
        this.map = new Character[DEFAULT_FLD_LENGTH][DEFAULT_FLD_WIDTH];

        this.initWorld(enemyConfiguration);
    }

    private void initWorld(List<EnemyConfiguration> enemyConfiguration) {
        initEnemies(enemyConfiguration);
        initMap();
    }


    void initEnemies(List<EnemyConfiguration> enemyConfiguration) {
        for (EnemyConfiguration configuration : enemyConfiguration) {
            Enemy enemy = new Enemy(configuration);
            Coordinates enemyCoordinates = randomCoordinatesWithoutAnyone();
            enemies.put(enemyCoordinates, enemy);
        }
    }

    void initMap() {
        for (int x = 0; x < map.length; x++) {
            Character[] column = map[x];
            for (int y = 0; y < column.length; y++) {
                Coordinates currentCoordinates = new Coordinates(x, y);
                Optional<NPC> someone = searchForNpc(currentCoordinates);
                if (someone.isPresent()) {
                    map[x][y] = someone.get();
                }
            }
        }
    }

    private Optional<NPC> searchForNpc(Coordinates currentCoordinates) {
        return Optional.ofNullable(enemies.get(currentCoordinates));
    }

    //TODO: think about a better way to do this...
    public Coordinates randomCoordinatesWithoutAnyone() {
        Coordinates coordinates;

        do {
            coordinates = randomCoordinates(map.length);
        } while (someoneIsThere(coordinates));

        return coordinates;
    }

    private boolean someoneIsThere(Coordinates coordinates) {
        return enemies.containsKey(coordinates);
    }

    static Coordinates randomCoordinates(int mapSize) {
        int randomX = randomIntExclusive(mapSize);
        int randomY = randomIntExclusive(mapSize);
        return new Coordinates(randomX, randomY);
    }


    public Character getLocation(Coordinates coordinates) {
        return getLocation(coordinates.getX(), coordinates.getY());
    }

    public Character getLocation(int x, int y) {
        validateCoordinates(x, y);
        return map[x][y];
    }

    public boolean allEnemiesDead() {
        return aliveEnemiesLeft() == 0;
    }

    public long aliveEnemiesLeft() {
        return enemies.values().stream().filter(Enemy::isAlive).count();
    }

    public String getName() {
        return name;
    }

    public Character[][] getMap() {
        return map;
    }

    public Map<Coordinates, Enemy> getEnemies() {
        return enemies;
    }

    public int mapSize() {
        return map.length;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("mapSize", mapSize())
                .build();
    }

    private Boolean validateCoordinates(int x, int y) {
        return validateCoordinate(x) && validateCoordinate(y);
    }

    private Boolean validateCoordinate(int index) {
        return isBetweenZeroAndMaxExclusive(index, mapSize());
    }

}
