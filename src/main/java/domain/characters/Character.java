package domain.characters;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

import static util.IntUtils.randomIntInclusive;

public abstract class Character implements Serializable {
    protected final String name;
    protected final String description;

    private static final Integer FIGHT_DAMAGE_VARIATION_MULTIPLIER = 2;

    //can change over time with exp
    protected int maxHp;
    protected int currentHp;
    protected int damage;
    //can change over time with exp
    protected int damageVariation;

    protected Coordinates coordinates;

    protected boolean isAlive;

    public Character(String name, String description, int maxHp, int damage, int damageVariation, Coordinates initialCoordinates) {
        this.name = name;
        this.description = description;

        this.maxHp = maxHp;
        this.currentHp = maxHp;
        this.damage = damage;
        this.damageVariation = damageVariation;

        this.isAlive = true;

        this.coordinates = initialCoordinates;
    }

    /**
     * deals damage to otherCharacter
     *
     * @param otherCharacter - enemy
     * @return damage dealt
     */
    public int attack(Character otherCharacter) {
        return otherCharacter.receiveDamage(calculateDamageToDeal());
    }

    private int receiveDamage(int damage) {
        int damageReceived = calculateDamageReceived(damage);
        currentHp -= damageReceived;
        if (currentHp <= 0) {
            die();
        }

        return damageReceived;
    }

    protected int calculateDamageToDeal() {
        return randomIntInclusive(damage, damage + damageVariation * FIGHT_DAMAGE_VARIATION_MULTIPLIER);
    }

    protected int calculateDamageReceived(int damage) {
        return damage;
    }

    public void die() {
        isAlive = false;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public int getCurrentHp() {
        return currentHp;
    }

    public int getDamage() {
        return damage;
    }

    public int getDamageVariation() {
        return damageVariation;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isDead() {
        return !isAlive;
    }

    public String currentStatus() {
        return "\n" + getClass().getSimpleName() + " current status is: \n" + toString();
    }

    @Override
    public String toString() {
        return toStringCommon().build();
    }

    private ToStringBuilder toStringCommon() {
        return new ToStringBuilder(this)
                .append("name " + name)
                .append("description " + description)
                .append("health" + currentHp + "/" + maxHp)
                .append("damage", damage + "-" + (damage + damageVariation * FIGHT_DAMAGE_VARIATION_MULTIPLIER));
    }
}
