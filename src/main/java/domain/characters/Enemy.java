package domain.characters;


import config.EnemyConfiguration;

public class Enemy extends NPC {

    public Enemy(String name, String description, String greeting, int maxHp, int damage,
                 int damageVariation, Coordinates initialCoordinates) {
        super(name, description, greeting, maxHp, damage, damageVariation, initialCoordinates);
    }

    public Enemy(EnemyConfiguration conf) {
        this(conf.getName(), conf.getDescription(), conf.getGreeting(), conf.getMaxHp(), conf.getDamage(),
                conf.getDamageVariation(), new Coordinates(0,0));
    }
}
