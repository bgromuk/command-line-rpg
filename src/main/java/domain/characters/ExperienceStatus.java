package domain.characters;

import static domain.characters.Experience.ENEMY_DEFEATED;
import static domain.characters.Experience.LEVEL_UP;

public enum ExperienceStatus {
    DIDNT_LEVEL_UP(ENEMY_DEFEATED),
    LEVELED_UP(ENEMY_DEFEATED + LEVEL_UP),
    DOUBLE_LEVELED_UP(ENEMY_DEFEATED + LEVEL_UP + "Twice!"),
    TRIPLE_LEVELED_UP(ENEMY_DEFEATED + LEVEL_UP + "Three times!");

    private final String desc;

    private ExperienceStatus(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
