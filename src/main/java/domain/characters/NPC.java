package domain.characters;

/**
 * NPC = Non Player Character
 * all variables loaded from some external configuration
 */
public abstract class NPC extends Character {
    private final String greeting;
    private final int expReward;

    public NPC(String name, String description, String greeting, int maxHp, int damage, int damageVariation, Coordinates coordinates) {
        super(name, description, maxHp, damage, damageVariation, coordinates);
        this.greeting = greeting;
        this.expReward = maxHp;
    }

    public int getExpReward() {
        return expReward;
    }

    public boolean isEnemy() {
        return this instanceof Enemy;
    }

    public String greeting() {
        return greeting;
    }
}
