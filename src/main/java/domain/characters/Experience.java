package domain.characters;


import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

import static domain.characters.ExperienceStatus.DIDNT_LEVEL_UP;

public class Experience implements Serializable {
    static final String ENEMY_DEFEATED = "Defeating this enemy gave you %d xp. ";
    static final String LEVEL_UP = "You earned enough xp to level up your hero! ";

    private int level;
    private int currentExp;
    private int lastLevelUpExp;

    int EXP_TO_FIRST_LEVEL_UP = 100;
    float NEXT_LEVEL_EXP_MULTIPLIER = 1.1f;


    public Experience() {
        this.currentExp = 0;
        this.level = 1;
    }

    public int getCurrentExp() {
        return currentExp;
    }

    public int getLevel() {
        return level;
    }

    public ExperienceStatus addKillReward(int expReward) {
        currentExp += expReward;
        int startingLevel = level;
        while (currentExp >= getExpRequiredToLevelUp()) {
            levelUp();
        }

        return fromLevelDiff(level - startingLevel);

    }

    int getExpRequiredToLevelUp() {
        if (1 == level) {
            return EXP_TO_FIRST_LEVEL_UP;
        } else {
            return (int) (lastLevelUpExp + lastLevelUpExp * NEXT_LEVEL_EXP_MULTIPLIER);
        }
    }

    private void levelUp() {
        lastLevelUpExp = getExpRequiredToLevelUp();
        level++;
    }

    private ExperienceStatus fromLevelDiff(int levelDiff) {
        for (ExperienceStatus experienceStatus : ExperienceStatus.values()) {
            if (experienceStatus.ordinal() == levelDiff) {
                return experienceStatus;
            }
        }

        return DIDNT_LEVEL_UP;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("level ", level)
                .append("currentExp", currentExp + "/" + getExpRequiredToLevelUp())
                .build();
    }
}
