package service;

import domain.characters.Character;
import domain.characters.Coordinates;
import domain.characters.Player;
import domain.field.Field;

public class NavigationService {
    private final Field field;
    private final Player player;


    public NavigationService(Field field, Player player) {
        this.field = field;
        this.player = player;
    }

    void travel(Coordinates coordinates) {
        Character character = field.getLocation(coordinates);
        if (character != null) {
            interactWithCharacter(character);
        } else {
            moveToEmptySpace(coordinates);
        }
    }

    private void interactWithCharacter(Character character) {
    }

    private void moveToEmptySpace(Coordinates coordinates) {
    }


}
